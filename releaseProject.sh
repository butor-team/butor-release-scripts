#!/bin/bash

[[ -z "$1" || ! -d "$1" ]] && {

echo "usage : Missing parameter / directory does not exist ! : project directory"
exit 1;

}
CWD=$(dirname $0)
PROJECT=$(readlink -f $1)


cd ${PROJECT}

mvn -U release:clean release:prepare release:perform -P sonatype-oss-release && hg push

cd ${CWD}
