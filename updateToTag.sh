#!/bin/bash

[[ -z "$1" ]] && {

echo "usage : Missing parameter : tag version number"
exit 1;

}


cat list_repo.txt | while read l
do
echo "============================= $l ============================="
cd $l
NAME=$(xmllint pom.xml --xpath "/*[local-name()='project']/*[local-name()='artifactId']/text()" | grep -v '/ >')

TAG="${NAME}-$1"
echo "Checkout tag ${TAG}"
hg pull -u 
hg checkout -C $REV
cd - &> /dev/null
done
