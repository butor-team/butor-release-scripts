#Butor Release Scripts

##Checkout the project

```
hg clone https://codingtony@bitbucket.org/butor-team/butor-release-scripts
```

### Initialize the workspace (only once)
```
cd butor-release-scripts
./cloneall.sh
```

####Instruction for releasing to Sonatype

You may want to check if all the project build together :
```
./mavenInstallAll.sh
```

To check if there is Non-SNAPSHOT version in the pom file you are releasing. Maybe it's the time to correct that (depending on the situation). e.g : 
```
./checkSnapshotVersion.sh butor-dao/
 -------
0.0.3-SNAPSHOT
 -------
0.0.4
```

Here we see that in butor-dao there is a non-snapshot release (the script checks only for com.butor groupId.)

Once you are ready, commit any modification to the project that is ready to publish
```
cd butor-dao
vi pom.xml
[...]
hg commit pom.xml -m "Updated butor-utils to 0.0.5-SNAPSHOT"
../releaseProject.sh .
```

The script issue a command to start the release process.


```
There are still some remaining snapshot dependencies.
: Do you want to resolve them now? (yes/no) no:
``` 
**yes**

```
Dependency type to resolve,: specify the selection number ( 0:All 1:Project Dependencies 2:Plugins 3:Reports 4:Extensions ): (0/1/2/3) 1: 
``` 
**PRESS ENTER**

```
Dependency 'com.butor:butor-utils' is a snapshot (0.0.5-SNAPSHOT)
: Which release version should it be set to? 0.0.5:
```
**0.9.0**

```
What version should the dependency be reset to for development? 0.9.0: :
``` 
**0.9.1-SNAPSHOT**

You repeat the process for all dependencies.
When you are done with the dependencies, Maven will ask you the version to deploy of the current project.


```
What is the release version for "Butor Framework - DAO Module"? (com.butor:butor-dao) 0.0.5: :
``` 
**0.9.0**


```
What is SCM release tag or label for "Butor Framework - DAO Module"? (com.butor:butor-dao) butor-dao-0.9.0: : 
```
**Press ENTER**

```
What is the new development version for "Butor Framework - DAO Module"? (com.butor:butor-dao) 0.9.1-SNAPSHOT: : 
``` 
**Press ENTER**


Maven will start to compile and test, it will ask you twice to provide your GPG passphrase in order to sign your release.

At the end of the release the script issues automatically an ```hg push``
You might need to provide your bitbucket passoword

#### If something goes wrong during the release (Maven questions)
```
cd butor-log4j
../releaseProject.sh .
CTRL-C during the questions (before the upload)
../cleanReleaseProject.sh .
```


### Release to Central

1. Login to : https://oss.sonatype.org/#welcome
2. Go to Staging Repositories
3. Look for combutor-xxxx in the Repository column
4. **Check the content** (Check down the screen in the content section).
5. Click on "close" when ready
6. When the verification is done : click on "Release" **THIS CANNOT BE UNDONE**
7. Drop if not good!!!
