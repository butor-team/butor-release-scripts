#!/bin/bash

[[ -z "$1" || ! -d "$1" ]] && {

echo "usage : Missing parameter / directory does not exist ! : project directory"
exit 1;

}
CWD=$(dirname $0)
PROJECT=$(readlink -f $1)


cd ${PROJECT}

xmllint pom.xml --shell <<<  "cat //*[local-name()='groupId'][text()='com.butor']/../*[local-name()='version']/text()" | grep -v '/ >'

cd ${CWD}
