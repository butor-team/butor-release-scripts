#!/bin/bash


PAGE=1
RC=0
ALL_URLS=""
while [[ ${RC} -eq 0 ]]
do
	URLS=$(curl -s "https://api.bitbucket.org/2.0/repositories/butor-team/?page=${PAGE}"  | jq '. values [] | .links .clone [] | select(.name=="ssh") .href' 2>/dev/null | grep -v null)
	RC=$?
	if [[ ${RC} -eq 0 ]]
	then
	ALL_URLS="${ALL_URLS} ${URLS}"
	fi
	PAGE=$(( PAGE + 1))
done
echo ${ALL_URLS} | tr ' ' '\n' | tr -d '"' | while read repo
do
hg clone ${repo}
done

