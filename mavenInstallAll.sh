#!/bin/bash

cat list_repo.txt | while read l
do
echo "============================= $l ============================="
cd $l
mvn -U clean install || exit 1
cd - &> /dev/null
done
