#!/bin/bash

cat list_repo.txt | while read l
do
cd $l
hg pull -u
hg checkout -C tip
cd -
done
